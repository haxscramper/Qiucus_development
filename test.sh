#!/usr/bin/env bash
# -*- coding: utf-8 -*-
# bash
cd "$(dirname "$0")"
set -o nounset
set -o errexit

top="qiucus-development"
start=$(pwd)/../$top
cd /tmp
rm -rf $top
cp -r $start .
cd $top
make git-init type=devel proj=source/qdelib
cp $0 source/qwgtlib
make git-status
make git-pack
