msg := $(shell  bash -c "((colecho --help > /dev/null && echo 'colecho -v ') \
2> /dev/null || echo 'echo -e \"\\033[91m[========]\033[0m: \"-- ')")

pushd-s := pushd > /dev/null
popd-s := popd > /dev/null


define check_software
($(1) --help > /dev/null && $(msg) -i1 $(1) 'is available') 2> /dev/null \
|| $(msg) $(if $(3),$(3),"-i1") $1 "is not available." $(2)
endef

define check_library
(ldconfig -p | grep )
endef

##@ Top-level makefile for qiucus-development repo

none: help
	@$(msg) -e0 "You need to specify target explicitly"

help: ##@ Print help message
	@sed -ne '/@sed/! s/##@ //p' ${MAKEFILE_LIST}

##= total

rebuild-total: ##@ Clean build *absolutely everything*: help utilities, libraries etc.
	@$(msg) -uw3 "Running total rebuild"
	@$(msg) -uw3 "All previously built libraries, apps, utilities will be deleted"
	@$(msg) -uw3 "All test cases will be ran"
	@$(msg) "top-level building hsupport"
	@{ cd source/haxscamper-misc/cpp/hsupport ; make -s rebuild-all ; }
	@$(msg) "top-level building qdelib"
	@{ cd source/qdelib ; make -s rebuild-all ; make -s run-all ; }
	@$(msg) "top-level building qwgtlib"
	@{ cd source/qwgtlib ; make -s rebuild-all ; }
	@echo " "

.PHONY: docs
docs:  ##@ Build documentation
	@mkdir -p docs
	@{ cd scripts ; \
		doxygen Doxyfile &> /dev/null ; \
		cd .. ; \
		( xclip -help &> /dev/null \
			&& echo $$(pwd)/docs/html/index.html | xclip -sel cli \
			&& $(msg) 'Path to docs has been copied to clipboard') || true ; \
		}

.PHONY: init
init: ##@ Check for software availability, versions etc.
	@$(msg) -i3 "Initiaizing ..."
	@$(msg) -i2 "Checking required software availability"
	@$(call check_software,ldconfig,"Critical for checking library \
	availability")
	@$(call check_software,colecho,"This is not critical but mesages \
	would be less colorful",-w1)
	@$(call check_software,doxygen,"You need it to generate documentation")
	@$(call check_software,nim,"You need this to build most of the internal \
	utilities",-w2)
	@$(call check_software,"git flow init","Required for development but \
	not needed for building")

##= Git

define git_get_submodules
$(shell git submodule--helper list | awk '{print $$4}')
endef

.PHONY: git-status
git-status:
	@$(msg) "Top-level"
	@git status -sb
	@$(foreach subm,$(call git_get_submodules), \
	{ \
		$(pushd-s) $(subm) ; \
		$(msg) $(subm) ; \
		echo 'status:' ; \
		git status -sb ; \
		echo 'branches:' ; \
		git branch -v ; \
		$(popd-s) ; \
	} ;)


gconf := "project_conf_git.toml"

.PHONY: git-init
git-init:
	@> $(gconf)
	@$(msg) -i3 "Initializing git"
ifeq ($(type), master)
	@$(msg) -i2 "Operation mode: master"
	@$(foreach subm,$(call git_get_submodules), \
	{ \
		$(pushd-s) $(subm) ; \
		git flow init -d &> /dev/null ; \
	  git checkout develop ; \
		$(popd-s) ; \
	} ;)
else ifeq ($(type), devel)
	@$(msg) -i2 "Operation mode: develop"
	@$(msg) -i1 "Target project:" $(proj)
	@$(foreach subm,$(call git_get_submodules), \
	{ \
		$(pushd-s) $(subm) ; \
		git flow init -d &> /dev/null ; \
	  git checkout develop &> /dev/null \
		|| ( git checkout -b develop &> /dev/null \
				&& $(msg) "created develop branch for" $(subm)); \
		git flow feature start dev-misc &> /dev/null ; \
		$(msg) "switched to feature/dev-misc for" $(subm) ; \
		$(popd-s) ; \
	} ;)
	@$git flow init -d &> /dev/null || true
	@$git flow feature start dev-misc &> /dev/null || true
	@$(msg) "switched to feature/dev-mics for master project"
	@echo 'operation_mode = "$(type)"' >> $(gconf)
	@echo 'target_project = "$(proj)"' >> $(gconf)
else
	@$(msg) -e2 "Unknown operation mode" $(type)
endif
	@cat $(gconf)

define get_val_from_toml ##@ Get value of the entry in .toml file
$(eval $(1) := $(shell grep $(1) $(2) | awk '{print $$3}'))
endef

.PHONY: git-pack
git-pack:
	@$(msg) "Packing changes"
	@$(call get_val_from_toml,target_project,$(gconf))
	@$(call get_val_from_toml,operation_mode,$(gconf))
	@git submodule--helper list \
	| awk '{print $$4}' \
	| grep -v $(target_project) \
	| xargs -i sh -c 'cd {} ; \
		git add . &> /dev/null ; \
		git commit -m "[???] Automatic commit" &> /dev/null ; \
		git format-patch develop -o .patch_dir; '
	@git submodule--helper list \
	| grep -v $(target_project) \
	| awk '{ print $$4; }' \
	| xargs -i bash -c 'arch=$$(echo {} \
		| sed "s!/!_!g").zip ; \
		rm -f $$arch ; \
		zip -r $$arch {}/.patch_dir/'
