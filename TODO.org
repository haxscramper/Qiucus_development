** TODO Guidelines [/]
*** TODO Write about [0/3]
    + [ ] Makefiles
      + [ ] How to name build steps
      + [ ] Documentation comments
    + [ ] Section comments
    + [ ] Documentation comments for languages that do not support
      them (perl,make,bash etc.)
*** TODO Organization [/]
    + [ ] Rewrite coding guidelines in org-mode instead of latex.
      Remove generated pdf.

** TODO README/CONTRIBUTING [/]
   + [ ] Update readme to include new makefile

** TODO Project organization [0/3]
   + [ ] Remove old scripts
   + [ ] Remove old notes from "notes" dir (move them to
     "notes-public" repo)
   + [ ] Ensure consistent naming of subdirectories
     - src/ :: for source code
     - include/ :: for public include headers
     - build/ :: temporary directory to run build in
     - lib/ :: temporary directory to put built libraries in
     - external/ :: not-mine submodules
     - notes/ :: textual notes
     - scripts/ :: scripts (to avoid cluttering top-level directory)
     + [ ] source -> src in master project

** TODO Build organization [/]
   + [ ] Make debug message printing optional (use autoconf?) across
     all projects.
   + [ ] Write consistent guidelines for when and where to use which
     levels of messages (colecho)

** TODO GIT organization [0/2]
   + [ ] Write documentation for commit message formatting
   + [ ] Add script to export differences from repository as patches
     (it can be integrated with global configuraion).
     + [ ] Need to make sure no modifcations are made to repository
       until it is configured to prevent mistakes when dealing with
       branches/patches. (possible solution: run script after cloning
       a repo and have different configuration profiles:
       user/developer (enable/disable debugging, to run or not to run
       tests/demo applications, support utilities))
   + [ ] Make cloning m.css optional (add check in ~make docs~?)
